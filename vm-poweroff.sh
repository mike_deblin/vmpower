﻿#!/bin/bash

export PERL_LWP_SSL_VERIFY_HOSTNAME=0

HOST="$1"
USER=localuser
PASS=password
LOG=/var/log/vm-power.log


if [ -z $HOST ]; then
echo "Укажите хост для соединения!"
exit 1;
fi

echo >> $LOG
echo "--- BEGIN POWER OFF ---" >> $LOG
echo `date`>> $LOG
echo "Hostname: " $HOST >> $LOG

vmware-cmd -l  -H $HOST -U $USER -P $PASS |awk -F '\r\n' '{if (NR>1) print $1}'| while read vmname; 
do 

VM="`echo $vmname | awk -F '/' '{print $6}'`"

echo $VM
#done
#exit

#LvMA.vmx собственно машинка на которой запускаем скрипт, т.е. она должна помереть последней
if [ "$VM" != 'LvMA.vmx' ]; then 

    STATUS=`vmware-cmd -q "$vmname" -H "$HOST" -U "$USER" -P "$PASS" getstate`;

    if [ $STATUS != '' ]; then
    echo -n  "$vmname - " >> $LOG;

        if [ $STATUS == 'on' ]; then 
		STAT=`vmware-cmd -q "$vmname" -H "$HOST" -U "$USER" -P "$PASS" stop soft`;

    	    if [ $? != 0 ]; then
			echo -n "Внимание! Выключаю принудительно! > " >> $LOG;
			STAT=`vmware-cmd -q  "$vmname" -H "$HOST" -U "$USER" -P "$PASS" stop hard`;
            fi;
	    if [ $STAT == 1 ]; then 
			echo "Выключен!" >> $LOG;
	    fi;
        fi;
    fi;
else

VM_ID="$vmname"

fi;


done

echo "Виртуалки выключены. Выключаем хост." >> $LOG

vicfg-hostops --server $HOST  --username $USER --password $PASS -o shutdown -h $HOST --force
#vicfg-hostops --server $HOST  --username $USER --password $PASS -o reboot -h $HOST --force

echo >> $LOG
echo "--- END POWER OFF ---" >> $LOG
echo >> $LOG
