﻿#!/bin/bash

export PERL_LWP_SSL_VERIFY_HOSTNAME=0

HOST=$1
USER=localuser
PASS=password
LOG=/var/log/vm-power.log


if [ -z $HOST ]; then 
echo "Укажите хост для соединения!"
exit 1;
fi


echo >> $LOG
echo "--- BEGIN POWER ON ---" >> $LOG
echo `date`>> $LOG
echo "Hostname: " $HOST >> $LOG

vmware-cmd -l  -H $HOST -U $USER -P $PASS |
awk -F '\r\n' '{if (NR>1) print $1}'|
while read vmname; 
do 
STATUS=`vmware-cmd -q "$vmname" -H $HOST -U $USER -P $PASS getstate`
echo -n  "$vmname - " >> $LOG;
if [ $STATUS == 'off' ]; then 
STAT=`vmware-cmd -q "$vmname" -H $HOST -U $USER -P $PASS start`
echo "Включен!" >> $LOG
fi;

done

echo >> $LOG
echo "--- END POWER ON ---" >> $LOG
echo >> $LOG
